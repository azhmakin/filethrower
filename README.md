FileThrower
by Andrey Zhmakin

A program to transmit files between computers within the same network.

I have written this program before I learned about ad-hoc FTP servers. So I suggest to check those out if you wish to transmit files between computers as my program is less convenient.

This program does not provide encryption or authentication of any sort. File integrity is checked with SHA-256 digest, but the transmission is subject to eavesdropping and man-in-middle attacks.

The 'default-hosts.txt' file contains a list of known hosts. You can use either an IP address or a computer name.
