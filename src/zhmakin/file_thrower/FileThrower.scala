package zhmakin.file_thrower

import java.awt.Component
import java.io.File
import javax.swing.{JFrame, JOptionPane}

import zhmakin.file_thrower.servers.Sender

/**
  * Companion object for FileThrower.
  * @author Andrey Zhmakin
  */
object FileThrower {
  def main(args: Array[String]) {
   val frame: JFrame = new JFrame("File Thrower")
   frame.setContentPane(new frmFileThrower().getContentPane)
   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
   frame.pack()
   frame.setLocationRelativeTo(null)
   frame.setVisible(true)
  }

  def send(file: File, target: String, parent: Component): Unit =
  {
    val sender: Sender
    = new Sender(file,
      target,
      () => { JOptionPane.showMessageDialog(parent, "Receiver is inaccessible!") })

    val thread: Thread = new Thread(sender)
    thread.start()
  }
 }
