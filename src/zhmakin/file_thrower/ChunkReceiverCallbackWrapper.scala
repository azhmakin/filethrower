package zhmakin.file_thrower

import javax.swing.JTable
import javax.swing.table.DefaultTableModel

import zhmakin.file_thrower.servers.Agent

/**
 * This class gets called on in initialisation of download and on receipt of every chunk.
 * @param tblTransmissions Table listing transmissions.
 * @author Andrey Zhmakin
 */
class ChunkReceiverCallbackWrapper(val tblTransmissions:JTable)
{
  def getCallback : Agent => Unit =
  {
    (cr : Agent) => {
      val model = tblTransmissions.getModel.asInstanceOf[DefaultTableModel]
      val v = new Array[AnyRef](1)
      v(0) = cr
      model.addRow(v)
      tblTransmissions.repaint()
    }
  }

  def getChunkCallback: Agent => Unit =
  {
    (cr : Agent) => {
      tblTransmissions.repaint()
    }
  }
}
