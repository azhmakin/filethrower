package zhmakin.file_thrower.messages

/**
 * This class represents transmission request message.
 * @param id Transmission id.
 * @param size Size of a file being transmitted.
 * @param port Port for future communications?
 * @param name File name.
 * @author Andrey Zhmakin
 */
class TransmissionRequestMessage(val id:Long, val size:Long, val port:Int, val name:String)
  extends BaseMessage
{
  override def toString: String =
  {
    "[id = " + id + ", size = " + size + "]"
  }
}
