package zhmakin.file_thrower.messages

/**
 * This class implements termination message; This message is send at the end of each file.
 * @author Andrey Zhmakin
 */
class TerminationMessage(val id:Long, val hash:Array[Byte])
  extends BaseMessage
{

}
