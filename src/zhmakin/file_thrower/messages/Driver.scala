package zhmakin.file_thrower.messages


import java.io.File

import zhmakin.file_thrower.servers.{Receiver, Sender}


/**
 * This class is used for testing purposes.
 * @author Andrey Zhmakin
 */
object Driver
{
  val TransmissionRequestPort = 12368

  val ChunkSize:Int = 256 * 1024

  val DigestAlgorithm = "SHA-256"


  def main(args: Array[String])
  {
    val senderThread = new Thread(new Sender(new File("../test-in.txt"), null, null))
    val receiverThread = new Thread(new Receiver)

    receiverThread.start()

    // TODO: Implement a delay by other means!
    for (i <- 1L to 100000L)
    {

    }

    senderThread.start()
  }
}
