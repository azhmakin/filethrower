package zhmakin.file_thrower.messages

/**
 * This class represents a transmission approval or rejection message.
 * @param id Transmission id.
 * @param port Port to hold future communications on; Send -1 for rejection.
 * @author Andrey Zhmakin
 */
class TransmissionApprovalMessage(val id:Long, val port:Int) extends BaseMessage
{

}
