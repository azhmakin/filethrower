package zhmakin.file_thrower.messages

import java.io.FileOutputStream

/**
 * This message carries a chunk of file data from sender to receiver.
 * @param fileId File identifier.
 * @param chunk Data being transmitted.
 * @param no Number of the data chunk.
 * @author Andrey Zhmakin
 */
class BlockMessage(val fileId:Long, val chunk:Array[Byte], val no:Long) extends BaseMessage
{
  /**
   * This method flushes the data to the stream specified.
   * @param fos File output stream.
   */
  def flush(fos:FileOutputStream): Unit =
  {
    fos.write(chunk)
  }
}
