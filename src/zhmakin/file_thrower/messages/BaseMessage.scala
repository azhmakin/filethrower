package zhmakin.file_thrower.messages

/**
 * This class is a base for message classes, the messages carry data between sender and receiver.
 * @author Andrey Zhmakin
 */
abstract class BaseMessage extends Serializable
{

}
