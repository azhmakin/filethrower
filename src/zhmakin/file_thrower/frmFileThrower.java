package zhmakin.file_thrower;


import scala.runtime.AbstractFunction0;
import scala.runtime.BoxedUnit;
import zhmakin.file_thrower.messages.Driver;
import zhmakin.file_thrower.servers.Agent;
import zhmakin.file_thrower.servers.Receiver;
import zhmakin.file_thrower.servers.Sender;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.EventObject;
import java.util.TooManyListenersException;


/**
 * @author Andrey Zhmakin
 */
public class frmFileThrower
{
    //--- Components ---
    private JPanel pnlSend;
    private JPanel pnlAddress;
    private JComboBox cmbAddress;
    private JButton cmdAddReceiver;
    private JTabbedPane tpnRoot;
    private JPanel pnlReceive;
    private JPanel pnlRoot;
    private JCheckBox chkListen;
    private JSpinner spnPort;
    private JTextPane txpLog;
    private JList lstReceivers;

    private JTable tblOutgoingTransmissions;
    private JPanel pnlTransmissions;
    private JTable tblIncomingTransmissions;

    //--- Internal ---
    private Receiver receiver;
    private ChunkReceiverCallbackWrapper chunkReceiverCallbackWrapper;
    private ChunkReceiverCallbackWrapper chunkSenderCallbackWrapper;
    private DefaultListModel<String> receiverListModel;


    public frmFileThrower()
    {
        this.initialize();

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader("./default-hosts.txt"));

            for ( ; ; )
            {
                String line = reader.readLine();

                if (line == null)
                {
                    break;
                }
                else
                {
                    line = line.trim();

                    if (!line.isEmpty())
                    {
                        this.addReceiver(line);
                    }
                }
            }
        }
        catch (FileNotFoundException e)
        {
            //e.printStackTrace();
            //this.addReceiver("localhost");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void configureTransmissionTable(JTable table)
    {
        table.setDefaultRenderer(Object.class, new TransmissionCellRenderer());
        table.setDefaultEditor(Object.class, new TransmissionCellEditor());
        table.setTableHeader(null);
        table.setBackground(Color.WHITE);
        table.setForeground(Color.WHITE);
        table.setGridColor(Color.WHITE);
        table.setRowSelectionAllowed(true);
        table.setDragEnabled(false);
        table.setRowHeight(40);
    }


    private void initialize()
    {
        spnPort.setValue(Driver.TransmissionRequestPort());

        this.lstReceivers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.lstReceivers.setLayoutOrientation(JList.VERTICAL);
        this.lstReceivers.setVisibleRowCount(-1);

        this.receiverListModel = new DefaultListModel<String>();
        this.lstReceivers.setModel(this.receiverListModel);

        this.configureTransmissionTable(this.tblOutgoingTransmissions);
        this.configureTransmissionTable(this.tblIncomingTransmissions);

        this.chunkReceiverCallbackWrapper = new ChunkReceiverCallbackWrapper(this.tblIncomingTransmissions);
        this.chunkSenderCallbackWrapper = new ChunkReceiverCallbackWrapper(this.tblOutgoingTransmissions);

        this.chkListen.addActionListener(
                e -> {
                    if (chkListen.isSelected())
                    {
                        String portStr = spnPort.getValue().toString().trim();
                        //System.out.println("portStr = " + portStr);
                        int port = Integer.parseInt(portStr);

                        // Start server
                        receiver = new Receiver(port,
                                                chunkReceiverCallbackWrapper.getCallback(),
                                                chunkReceiverCallbackWrapper.getChunkCallback());
                        Thread thread = new Thread(receiver);
                        thread.start();

                        spnPort.setEnabled(false);
                    }
                    else
                    {
                        // Stop server
                        receiver.interrupt();
                        spnPort.setEnabled(true);
                    }
                }
        );

        this.cmdAddReceiver.addActionListener(
                e -> {
                    String address = (String) cmbAddress.getSelectedItem();

                    addReceiver(address);
                }
        );

        DropTarget dt = new DropTarget();

        try
        {
            dt.addDropTargetListener(
                    new DropTargetListener()
                    {
                        @Override
                        public void dragEnter(DropTargetDragEvent dtde) { }

                        @Override
                        public void dragOver(DropTargetDragEvent dtde) { }

                        @Override
                        public void dropActionChanged(DropTargetDragEvent dtde) { }

                        @Override
                        public void dragExit(DropTargetEvent dte) { }

                        @Override
                        public void drop(DropTargetDropEvent dtde)
                        {
                            Transferable tf = dtde.getTransferable();

                            try
                            {
                                // TODO: Check data flavour, then accept!
                                dtde.acceptDrop(DnDConstants.ACTION_COPY);

                                Object droppedObject = tf.getTransferData(DataFlavor.javaFileListFlavor);

                                java.util.List<File> fileList = (java.util.List<File>) droppedObject;

                                String target = (String) lstReceivers.getSelectedValue();

                                for (File file : fileList)
                                {
                                    Sender sender
                                        = new Sender(file,
                                            target,
                                            new AbstractFunction0<BoxedUnit>() {
                                                @Override
                                                public BoxedUnit apply() {
                                                    JOptionPane.showMessageDialog(getContentPane(),
                                                            "Receiver is inaccessible!",
                                                            "Error",
                                                            JOptionPane.ERROR_MESSAGE);
                                                    return null;
                                                }
                                            },
                                            chunkSenderCallbackWrapper.getChunkCallback());

                                    chunkSenderCallbackWrapper.getCallback().apply(sender);

                                    Thread thread = new Thread(sender);
                                    thread.start();
                                }
                            }
                            catch (UnsupportedFlavorException e)
                            {
                                dtde.rejectDrop();
                            }
                            catch (IOException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });
        }
        catch (TooManyListenersException e)
        {
            e.printStackTrace();
        }

        this.pnlTransmissions.setDropTarget(dt);
    }


    private static final class TransmissionCellRenderer
            extends BaseCellRenderer
            implements TableCellRenderer
    {
        TransmissionCellRenderer()
        {
            super();
        }

        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column)
        {
            Agent agent = (Agent) value;

            this.setProgress(agent);

            return this;
        }
    }


    final ActionListener cancelClearActionListener =
            e -> {
                JTable table = null;

                switch (tpnRoot.getSelectedIndex())
                {
                    case 0: table = tblOutgoingTransmissions; break;
                    case 1: table = tblIncomingTransmissions; break;
                }

                assert table != null;

                int selectedRow = table.getSelectedRow();

                if (selectedRow == -1)
                {
                    return;
                }

                Agent agent = (Agent) table.getModel().getValueAt(selectedRow, 0);

                if (agent.isComplete())
                {
                    ((DefaultTableModel) table.getModel()).removeRow(selectedRow);
                }
                else
                {
                    int rc = JOptionPane.showConfirmDialog(table,
                            "Do you want to abort transmission",
                            "Confirm",
                            JOptionPane.YES_NO_OPTION);

                    if (rc == JOptionPane.YES_OPTION)
                    {
                        agent.abort();
                    }
                }
            };


    private final class TransmissionCellEditor
        extends BaseCellRenderer
        implements TableCellEditor
    {
        TransmissionCellEditor()
        {
            super();

            this.cmdCancelClear.addActionListener(cancelClearActionListener);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column)
        {
            Agent agent = (Agent) value;

            this.setProgress(agent);

            return this;
        }

        @Override
        public Object getCellEditorValue() {
            return null;
        }

        @Override
        public boolean isCellEditable(EventObject anEvent) {
            return true;
        }

        @Override
        public boolean shouldSelectCell(EventObject anEvent) {
            return true;
        }

        @Override
        public boolean stopCellEditing() {
            return false;
        }

        @Override
        public void cancelCellEditing() {

        }

        @Override
        public void addCellEditorListener(CellEditorListener l) {

        }

        @Override
        public void removeCellEditorListener(CellEditorListener l) {

        }
    }


    private static abstract class BaseCellRenderer
        extends JPanel
    {
        JProgressBar progressBar;
        JButton cmdCancelClear;

        BaseCellRenderer()
        {
            this.progressBar = new JProgressBar();

            this.progressBar.setMinimum(0);
            this.progressBar.setMaximum(1000);

            this.progressBar.setStringPainted(true);

            this.add(this.progressBar, BorderLayout.CENTER);

            this.cmdCancelClear = new JButton("X");
            this.add(this.cmdCancelClear, BorderLayout.EAST);
        }

        final void setProgress(Agent receiver)
        {
            int promillage = (int) Math.round( 1000.0 * receiver.blockCount() / receiver.totalBlocks() );
            this.progressBar.setValue(promillage);
            this.progressBar.setString(promillage + "‰");
        }
    }


    public JPanel getContentPane()
    {
        return this.pnlRoot;
    }


    private void addReceiver(String address)
    {
        this.receiverListModel.insertElementAt(address, this.receiverListModel.size());
    }


    private class ReceiverList
        extends JList
    {
        public ReceiverList()
        {
            super();
        }
    }


    private void createUIComponents()
    {
        this.lstReceivers = new ReceiverList();
        this.tblOutgoingTransmissions = new JTable(new DefaultTableModel(0, 1));
        this.tblIncomingTransmissions = new JTable(new DefaultTableModel(0, 1));
    }
}
