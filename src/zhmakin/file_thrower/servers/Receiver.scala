package zhmakin.file_thrower.servers

import java.io._
import java.net.{ServerSocket, Socket, SocketException}
import java.security.MessageDigest
import javax.swing.{JFileChooser, JOptionPane}

import zhmakin.file_thrower.messages._

/**
 * This class implements logic of receiver server.
 * @param port A port to listen transmission requests from.
 * @author Andrey Zhmakin
 */
class Receiver(val port : Int = Driver.TransmissionRequestPort,
               val transmissionAcceptedCallback : Agent => Unit = null,
               val chunkReceivedCallback: Agent => Unit = null) extends Runnable
{
  var runs = true
  var serverSocket: ServerSocket = _

  override def run(): Unit =
  {
    // Listen for transmission request
    serverSocket = new ServerSocket(this.port)

    // Listen for incoming transmission requests
    while (this.runs)
    {
      try
      {
        val clientSocket = serverSocket.accept()

        val objectInputStream = new ObjectInputStream(clientSocket.getInputStream)

        val message = objectInputStream.readObject()

        message match {
          case request: TransmissionRequestMessage =>

            //System.out.println("Transmission request received!"); System.out.flush()

            val acceptOption = JOptionPane.showConfirmDialog(null,
              "Do you want to receive a file " + request.name + " of size " + request.size + " byte(s) from "
                + clientSocket.getInetAddress + " ?",
              "Warning",
              JOptionPane.YES_NO_OPTION)

            // This is necessary to send the response
            val backwardSocket = new Socket(clientSocket.getInetAddress, request.port)
            val objectOutputStream = new ObjectOutputStream(backwardSocket.getOutputStream)
            // Prepare to receive incoming messages at some port
            val ss = new ServerSocket(0)

            acceptOption match {
              case JOptionPane.YES_OPTION =>
                val fc = new JFileChooser()
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY)
                fc.setMultiSelectionEnabled(false)
                fc.setSelectedFile(new File(request.name))

                val rcResult = fc.showSaveDialog(null)

                val targetFileName = fc.getSelectedFile.getAbsolutePath

                val chunkReceiver
                  = new ChunkReceiver(ss,
                    request,
                    new FileOutputStream(new File(targetFileName)),
                    chunkReceivedCallback)

                if (transmissionAcceptedCallback != null)
                {
                  transmissionAcceptedCallback(chunkReceiver)
                }

                // Start thread that will receive data
                val uploadThread = new Thread(chunkReceiver)

                uploadThread.start()

                // Now we are ready to receive data from sender

                // Prepare and send transmission approval
                objectOutputStream.writeObject(new TransmissionApprovalMessage(request.id, ss.getLocalPort))
                objectOutputStream.close()
                backwardSocket.close()

              case JOptionPane.NO_OPTION =>
              // Send transmission rejection message
                objectOutputStream.writeObject(new TransmissionApprovalMessage(request.id, -1))
                objectOutputStream.close()
                backwardSocket.close()
                ss.close()
            }

          case _ =>
        }

        objectInputStream.close()
        clientSocket.close()
      }
      catch
      {
        case sce : SocketException =>
          assert(!this.runs, "Loop flag should be dropped by now!")
      }
    }

      serverSocket.close()
  }

  def interrupt(): Unit =
  {
    this.runs = false
    this.serverSocket.close()
  }
}


/**
 * This class is responsible for saving incoming data into file.
 * @param serverSocket Server socket to get data from.
 * @param request Request.
 * @param fos File to write data to.
 */
class ChunkReceiver(val serverSocket:ServerSocket,
                    val request:TransmissionRequestMessage,
                    val fos:FileOutputStream,
                    val chunkReceivedCallback : ChunkReceiver => Unit = null)
  extends Runnable
  with Agent
{
  var blockCount:Long = 0
  var totalBlocks:Long = (request.size / Driver.ChunkSize) + (if (request.size % Driver.ChunkSize != 0) 1 else 0)
  var isComplete = false

  private var inLoop = false

  override def run(): Unit =
  {
    val socket = serverSocket.accept()

    val ois = new ObjectInputStream(socket.getInputStream)

    val hash = MessageDigest.getInstance(Driver.DigestAlgorithm)
    hash.reset()

    this.inLoop = true
    this.blockCount = 0

    while (this.inLoop)
    {
      try
      {
        val message = ois.readObject()
        //ois.reset()

        message match {
          case envelop : BlockMessage =>
            //println("envelop.chunk.length = " + envelop.chunk.length)

            hash.update(envelop.chunk, 0, envelop.chunk.length)
            fos.write(envelop.chunk, 0, envelop.chunk.length)
            fos.flush()

            assert(envelop.no == this.blockCount, "envelop.no = " + envelop.no + ", blockCount = " + this.blockCount)
            this.blockCount += 1

            if (chunkReceivedCallback != null)
            {
              chunkReceivedCallback(this)
            }

          case terminator : TerminationMessage =>
            val digest = hash.digest()
            //println("Received hash: " + toHex(terminator.hash))
            //println("Local hash: " + toHex(digest))

            assert(toHex(digest) == toHex(terminator.hash))
            this.inLoop = false

          case _ =>
        }
      }
      catch
        {
          case e : EOFException =>
            //println("Time to exit envelop processing loop!")
            this.inLoop = false
        }
    }

    fos.flush()
    fos.close()

    ois.close()

    socket.close()

    serverSocket.close()

    this.isComplete = true
  }


  def abort(): Unit =
  {
    this.inLoop = false
  }


  def toHex(a : Array[Byte]) : String =
  {
    val digit = Array( '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' )

    val buffer = new StringBuilder(a.length * 2)

    for (b <- a)
    {
      buffer.append( digit( (b >> 4) & 0x0F ) )
      buffer.append( digit(  b       & 0x0F ) )
    }

    buffer.mkString
  }
}
