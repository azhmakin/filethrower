package zhmakin.file_thrower.servers

import java.io.{File, FileInputStream, ObjectInputStream, ObjectOutputStream}
import java.net.{ConnectException, InetAddress, ServerSocket, Socket}
import java.security.{MessageDigest, SecureRandom}
import javax.swing.JOptionPane

import zhmakin.file_thrower.messages._

/**
 * This class contains routines necessary to send file to the receiver.
 * @param file File to send.
 * @param target Address of target receiver.
 * @author Andrey Zhmakin
 */
class Sender(val file : File,
             val target : String,
             val connectionRefusedCallback : () => Unit = null,
             val chunkSentCallback : Agent => Unit = null) extends Runnable with Agent
{
  var blockCount  : Long = 0
  var totalBlocks : Long = 0

  /**
   * True if transmission is complete or aborted.
   */
  var isComplete  : Boolean = false

  private var isNotInterrupted : Boolean = true


  override def run(): Unit =
  {
    val serverAddress = InetAddress.getByName(target)

    isComplete = false
    isNotInterrupted = true

    // Create sockets

    var requestSocket:Socket = null

    try
    {
      requestSocket = new Socket(serverAddress, Driver.TransmissionRequestPort)
    }
    catch
    {
      case cex: ConnectException =>
        // Catch "java.net.ConnectException: Connection refused: connect" when receiver is not accessible
        connectionRefusedCallback.apply()
        isComplete = true
        return
    }

    val approvalServerSocket = new ServerSocket(0)

    // Send transmission request
    val objectOutputStream = new ObjectOutputStream( requestSocket.getOutputStream )
    val random = new SecureRandom()

    val transmissionId = random.nextLong()

    objectOutputStream.writeObject(
      new TransmissionRequestMessage( transmissionId,
        file.length(),
        approvalServerSocket.getLocalPort,
        file.getName ) )
    requestSocket.close()
    //System.out.println("Transmission request sent!"); System.out.flush()

    // Get transmission approval
    val approvalSocket = approvalServerSocket.accept()
    val objectInputStream = new ObjectInputStream(approvalSocket.getInputStream)
    val message = objectInputStream.readObject()
    objectInputStream.close()
    approvalSocket.close()
    approvalServerSocket.close()

    message match {
      case approval: TransmissionApprovalMessage =>
        if (approval.port == -1)
        {
          //println("Transmission rejected!")
          JOptionPane.showMessageDialog(null, "Transmission rejected!", "Warning", JOptionPane.OK_OPTION)
        }
        else
        {
          //System.out.println("Approval received, port = " + approval.port); System.out.flush()

          assert(approval.id == transmissionId, "Transmission IDs must match!")

          // Transmit file contents
          val transmissionSocket = new Socket(serverAddress, approval.port)
          val envelopStream = new ObjectOutputStream(transmissionSocket.getOutputStream)

          val fis = new FileInputStream(file)

          val buffer = new Array[Byte](Driver.ChunkSize)

          var hasBytes = true

          val hash = MessageDigest.getInstance(Driver.DigestAlgorithm)
          hash.reset()

          this.blockCount = 0
          this.totalBlocks
            = file.length() / Driver.ChunkSize + (if (file.length() % Driver.ChunkSize == 0) 0 else 1)

          while (hasBytes && isNotInterrupted)
          {
            val bytesRead = fis.read(buffer, 0, Driver.ChunkSize)

            hasBytes = bytesRead > 0

            if (hasBytes)
            {
              //System.out.print("Sending chunk of data..."); System.out.flush()

              hash.update(buffer, 0, bytesRead)

              var data: Array[Byte] = null

              // TODO: Find a way to reuse memory
              // Wait for flush!

              if (bytesRead == Driver.ChunkSize)
              {
                data = buffer
              }
              else
              {
                data = new Array[Byte](bytesRead)

                for (i <- 0 until bytesRead)
                {
                  data(i) = buffer(i)
                }
              }

              envelopStream.writeObject(new BlockMessage(transmissionId, data, this.blockCount))
              envelopStream.flush()
              envelopStream.reset()

              this.blockCount += 1

              if (chunkSentCallback != null)
              {
                chunkSentCallback(this)
              }

              //System.out.println("Done!"); System.out.flush()
            }
          }

          if (this.isNotInterrupted)
          {
            val digest = hash.digest()
            //println("Sending hash: " + Driver.toHex(digest))
            envelopStream.writeObject(new TerminationMessage(transmissionId, digest))
          }

          fis.close()

          envelopStream.close()
          transmissionSocket.close()
        }

      case _ =>
    }

    isComplete = true
  }


  def abort() : Unit =
  {
    this.isNotInterrupted = false
  }
}
