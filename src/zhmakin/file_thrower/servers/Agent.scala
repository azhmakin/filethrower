package zhmakin.file_thrower.servers

/**
 * Sender or receiver capable of reporting its progress and aborting.
 * @author Andrey Zhmakin
 */
trait Agent
{
  def blockCount : Long
  def totalBlocks : Long
  def isComplete : Boolean
  def abort()
}
